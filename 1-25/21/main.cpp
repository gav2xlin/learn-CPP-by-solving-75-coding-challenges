#include <iostream>
#include <string>

using namespace std;

std::string newWord(std::string str){
  return str.substr(1);
}

int main() {
  std::cout << newWord("apple") << std::endl;
  std::cout << newWord("cherry") << std::endl;
  std::cout << newWord("plum") << std::endl;
}
