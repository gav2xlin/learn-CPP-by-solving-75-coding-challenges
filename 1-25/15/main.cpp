#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> nextInLine(std::vector<int> que, int num) {
    // que.emplace_back(num);
    // return std::vector<int>(begin(que) + 1,end(que));
    que.push_back(num);
    que.erase(que.begin());
    return que;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << nextInLine({5, 6, 7, 8, 9}, 1) << std::endl;
    std::cout << nextInLine({7, 6, 3, 23, 17}, 10) << std::endl;
    std::cout << nextInLine({1, 10, 20, 42}, 6) << std::endl;
    std::cout << nextInLine({}, 6) << std::endl;
}
