#include <iostream>

using namespace std;

std::string has_bugs(bool buggy_code) {
    if (buggy_code) {
        return "sad days";
    } else {
        return "it's a good day";
    }
}

int main() {
    // this exercies contained the function with simple bugs
    std::cout << std::boolalpha << has_bugs(true) << std::endl;
    std::cout << std::boolalpha << has_bugs(false) << std::endl;
}
