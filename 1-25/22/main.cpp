#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool hasSpaces(std::string str) {
  // return str.find(' ') != std::string::npos;
  return std::count_if(str.begin(), str.end(), [](char c) {
      return c == ' ';
  });
}

int main() {
  std::cout << std::boolalpha << hasSpaces("hello") << std::endl; // false
  std::cout << hasSpaces("hello, world") << std::endl; // false
  std::cout << hasSpaces(" ") << std::endl; // true
  std::cout << hasSpaces("") << std::endl; // true
  std::cout << hasSpaces(",./!@#") << std::endl; // false
}
