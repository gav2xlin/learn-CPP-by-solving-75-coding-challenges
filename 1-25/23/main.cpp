#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> indexOfCaps(std::string str) {
    std::vector<int> r;
    for (int i = 0; i < str.size(); ++i) {
        // #include <cctype>
        // isupper(str[i)
        if ('A' <= str[i] && str[i] <= 'Z') r.push_back(i);
    }
    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << indexOfCaps("eDaBiT") << std::endl; // 1, 3, 5
    std::cout << indexOfCaps("eQuINoX") << std::endl; // 1, 3, 4, 6
    std::cout << indexOfCaps("determine") << std::endl; //
    std::cout << indexOfCaps("STRIKE") << std::endl; // 0, 1, 2, 3, 4, 5
    std::cout << indexOfCaps("sUn") << std::endl; // 1
}
