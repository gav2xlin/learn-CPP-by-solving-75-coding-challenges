#include <iostream>
#include <string>

using namespace std;

std::string modifyLast(std::string str, int n) {
  // std::string rep(n - 1, str.back());
  // return str + rep;
  if (!str.empty() && n > 0) {
      auto sz = str.length();
      str.resize(sz + n - 1);
      std::fill(str.begin() + sz - 1, str.end(), str[sz - 1]);
  }
  return str;
}

int main() {
  std::cout << modifyLast("Hello", 3) << std::endl;
  std::cout << modifyLast("hey", 6) << std::endl;
  std::cout << modifyLast("excuse me what?", 5) << std::endl;
}
