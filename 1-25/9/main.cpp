#include <iostream>

using namespace std;

int carsNeeded(int n) {
    int c = n / 5;
    if (n % 5 != 0) ++c;
    return c;
}

int main() {
    std::cout << carsNeeded(5) << std::endl; // 1
    std::cout << carsNeeded(11) << std::endl; // 3
    std::cout << carsNeeded(0) << std::endl; // 0
}
