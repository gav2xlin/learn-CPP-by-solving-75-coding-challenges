#include <iostream>
#include <string>

using namespace std;

bool isSame(int a, int b) {
    // return (a == 0 && b == 0) || a*b>0;
    return a < 0 && b < 0 || a > 0 && b > 0 || a == 0 && b == 0;
}

int main() {
    std::cout << std::boolalpha << isSame(6, 2) << std::endl; // true
    std::cout << isSame(0, 0) << std::endl; // true
    std::cout << isSame(-1, 2) << std::endl; // false
    std::cout << isSame(0, 2) << std::endl; // false
}
