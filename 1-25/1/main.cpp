#include <iostream>

using namespace std;

int addition(int a, int b) {
  return a + b;
}

int main() {
    std::cout << addition(3, 2) << std::endl; // 5
    std::cout << addition(-3, -6) << std::endl; // 9
    std::cout << addition(7, 3) << std::endl; // 10
}
