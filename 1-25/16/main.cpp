#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<int> addIndexes(vector<int> arr) {
    for (int i = 0; i < arr.size(); ++i) {
        auto &v = arr[i];
        v += i;
    }
    return arr;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << addIndexes({0, 0, 0, 0, 0}) << std::endl;
    std::cout << addIndexes({1, 2, 3, 4, 5}) << std::endl;
    std::cout << addIndexes({5, 4, 3, 2, 1}) << std::endl;
}
