#include <iostream>
#include <string>

using namespace std;

int countDs(std::string sentence) {
    // #include <algorithm>
    // std::count(sentence.begin(),sentence.end(),'d')
    // + std::count(sentence.begin(),sentence.end(),'D'));

    int r = 0;
    for (char ch : sentence) {
        if (ch == 'D' || ch == 'd') ++r;
    }
    return r;
}

int main() {
    std::cout << countDs("My friend Dylan got distracted in school.") << std::endl; // 4
    std::cout << countDs("Debris was scattered all over the yard.") << std::endl; // 3
    std::cout << countDs("The rodents hibernated in their den.") << std::endl; // 3
}
