#include <iostream>
#include <string>

using namespace std;

std::string removeFirstLast(std::string str) {
    return str.substr(1, str.size() - 2);
}

int main() {
    std::cout << removeFirstLast("hello") << std::endl;
    std::cout << removeFirstLast("maybe") << std::endl;
    std::cout << removeFirstLast("benefit") << std::endl;
}
