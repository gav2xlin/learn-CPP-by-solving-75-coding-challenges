#include <iostream>
#include <string>
#include <vector>

using namespace std;

int sumFive(std::vector<int> arr) {
    int sum = 0;
    for (auto v : arr) {
        if (v > 5) sum += v;
    }
    return sum;
}

int main() {
    std::cout << sumFive({1, 5, 20, 30, 4, 9, 18}) << std::endl; // 77
    std::cout << sumFive({1, 2, 3, 4}) << std::endl; // 0
    std::cout << sumFive({10, 12, 28, 47, 55, 100}) << std::endl; // 252
}
