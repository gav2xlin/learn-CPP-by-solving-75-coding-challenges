#include <iostream>
#include <string>

using namespace std;

std::string pHName(float pH) {
  if (pH < 0 || pH > 14)
    return "invalid";
  else if (pH > 7)
    return "alkaline";
  else if (pH < 7)
    return "acidic";
  else
    return "neutral";
}

int main() {
  std::cout << pHName(5) << std::endl; // acidic
  std::cout << pHName(8.7) << std::endl; // alkaline
  std::cout << pHName(7) << std::endl; // neutral
}
