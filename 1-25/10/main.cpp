#include <iostream>
#include <string>

using namespace std;

bool isEmpty(std::string str) {
    return str == "";
}

int main() {
    std::cout << std::boolalpha << isEmpty("") << std::endl; // true
    std::cout << isEmpty(" ") << std::endl; // false
    std::cout << isEmpty("a") << std::endl; // false
}
