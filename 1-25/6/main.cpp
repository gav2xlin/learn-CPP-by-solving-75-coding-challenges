#include <iostream>

using namespace std;

bool divisibleByHundred(int num) {
  return !(num % 100);
}

int main() {
    std::cout << std::boolalpha << divisibleByHundred(1) << std::endl; // false
    std::cout << std::boolalpha << divisibleByHundred(1000) << std::endl; // true
    std::cout << std::boolalpha << divisibleByHundred(100) << std::endl; // true
}
