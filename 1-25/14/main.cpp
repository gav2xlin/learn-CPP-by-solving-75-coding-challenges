#include <iostream>
#include <string>

using namespace std;

bool isLastCharacterN(std::string w) {
    return w.back() == 'n';
}

int main() {
    std::cout << std::boolalpha << isLastCharacterN("Aiden") << std::endl; // true
    std::cout << isLastCharacterN("Piet") << std::endl; // false
    std::cout << isLastCharacterN("Bert") << std::endl; // false
    std::cout << isLastCharacterN("Dean") << std::endl; // true
}
