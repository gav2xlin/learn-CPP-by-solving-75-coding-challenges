#include <iostream>
#include <string>

using namespace std;

int clubEntry(std::string word) {
    int pass = 0;
    for (int i = 0; i < word.size() - 1; ++i) {
      if (word[i] == word[i + 1]) {
        pass = (word[i] - 'a' + 1) * 4;
      }
    }
    return pass;
}

int main() {
  std::cout << clubEntry("hill") << std::endl; // 48
  std::cout << clubEntry("apple") << std::endl; // 64
  std::cout << clubEntry("bee") << std::endl; // 20
}
