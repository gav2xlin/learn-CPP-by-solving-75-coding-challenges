#include <iostream>

using namespace std;

std::string giveMeSomething(std::string a) {
  return "something " + a;
}

int main() {
    std::cout << giveMeSomething("is better than nothing") << std::endl;
    std::cout << giveMeSomething("Bob Jane") << std::endl;
    std::cout << giveMeSomething("something") << std::endl;
}
