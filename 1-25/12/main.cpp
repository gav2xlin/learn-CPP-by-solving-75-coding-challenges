#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> invertArray(std::vector<int> arr) {
    for (auto &d : arr) {
        d = -d;
    }
    return arr;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << invertArray({1, 2, 3, 4, 5}) << std::endl;
    std::cout << invertArray({1, -2, 3, -4, 5}) << std::endl;
    std::cout << invertArray({}) << std::endl;
}
