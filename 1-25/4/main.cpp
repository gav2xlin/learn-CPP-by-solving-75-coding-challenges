#include <iostream>

using namespace std;

bool dividesEvenly(int a, int b) {
    return b != 0 ? !(a % b) : false;
}

int main() {
    std::cout << std::boolalpha << dividesEvenly(98, 7) << std::endl; // true
    std::cout << dividesEvenly(85, 4) << std::endl; // false
}
