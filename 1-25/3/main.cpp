#include <iostream>

using namespace std;

bool lessThan100(int a, int b) {
    return a + b < 100;
}

int main() {
    std::cout << std::boolalpha << lessThan100(22, 15) << std::endl; // true
    std::cout << lessThan100(83, 34) << std::endl; // false
    std::cout << lessThan100(3, 77) << std::endl; //true
}
