#include <iostream>

using namespace std;

bool isEqual(int num1, int num2) {
    return num1 == num2;
}

int main() {
    std::cout << std::boolalpha << isEqual(5, 6) << std::endl; // false
    std::cout << isEqual(1, 1) << std::endl; // true
    std::cout << isEqual(36, 35) << std::endl; // false
}
