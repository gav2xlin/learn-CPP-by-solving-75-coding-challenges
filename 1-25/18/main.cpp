#include <iostream>
#include <string>

using namespace std;

// the function must be recursive
int length(std::string str) {
    return str == "" ? 0 : 1 + length(str.substr(1));
}

int main() {
    std::cout << length("apple") << std::endl; // 5
    std::cout << length("make") << std::endl; // 4
    std::cout << length("a") << std::endl; // 1
    std::cout << length("") << std::endl; // 0
}
