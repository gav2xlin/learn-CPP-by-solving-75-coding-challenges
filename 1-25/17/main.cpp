#include <iostream>
#include <string>
#include <vector>

using namespace std;

int findIndex(vector<string> arr, string str) {
    for (int i = 0; i < arr.size(); ++i) {
        if (arr[i] == str) return i;
    }
    return -1;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << findIndex({"hi", "edabit", "fgh", "abc"}, "fgh") << std::endl; // 2
    std::cout << findIndex({"Red", "blue", "Blue", "Green"}, "blue") << std::endl; // 1
    std::cout << findIndex({"a", "g", "y", "d"}, "d") << std::endl; // 3
    std::cout << findIndex({"Pineapple", "Orange", "Grape", "Apple"}, "Pineapple") << std::endl; // 0
}
