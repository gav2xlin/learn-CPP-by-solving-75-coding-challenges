#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> arrayOfMultiples(int num, int length) {
    // #include <numeric>
    // std::vector<int> mul(length, num);
    // std::partial_sum(mul.begin(), mul.end(), mul.begin());
    std::vector<int> r(length);
    for (int i = 0; i < length; ++i) {
        r[i] = (i + 1) * num;
    }
    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << arrayOfMultiples(7, 5) << '\n';
    std::cout << arrayOfMultiples(12, 10) << '\n';
    std::cout << arrayOfMultiples(17, 6) << '\n';
}
