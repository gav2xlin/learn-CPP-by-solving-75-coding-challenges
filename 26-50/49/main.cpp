#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int letterCounter(vector<vector<char>> arr, char c) {
    int r = 0;
    for(const auto &v : arr) {
        r += count(v.begin(), v.end(), c);
    }
    return r;
}

int main() {
    std::cout << letterCounter({{'D', 'E', 'Y', 'H', 'A', 'D'},
                                {'C', 'B', 'Z', 'Y', 'J', 'K'},
                                {'D', 'B', 'C', 'A', 'M', 'N'},
                                {'F', 'G', 'G', 'R', 'S', 'R'},
                                {'V', 'X', 'H', 'A', 'S', 'S'}}, 'D') << std::endl;
    std::cout << letterCounter({{'D', 'E', 'Y', 'H', 'A', 'D'},
                                {'C', 'B', 'Z', 'Y', 'J', 'K'},
                                {'D', 'B', 'C', 'A', 'M', 'N'},
                                {'F', 'G', 'G', 'R', 'S', 'R'},
                                {'V', 'X', 'H', 'A', 'S', 'S'}}, 'H') << std::endl;
}
