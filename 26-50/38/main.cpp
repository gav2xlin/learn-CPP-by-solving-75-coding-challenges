#include <iostream>
#include <string>
#include <regex>

using namespace std;

std::string removeSpecialCharacters(std::string str) {
  return std::regex_replace(str, std::regex("[^A-Za-z0-9\\-_ ]"), "");
}

int main() {
    std::cout << removeSpecialCharacters("The quick brown fox!") << std::endl;
    std::cout << removeSpecialCharacters("$fd76$fd(-)6GvKl0.") << std::endl;
    std::cout << removeSpecialCharacters("D0n$c sed 0di0 du1") << std::endl;
}
