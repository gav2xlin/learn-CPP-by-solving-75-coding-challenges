#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

std::vector<int> noOdds(std::vector<int> arr) {
    std::vector<int> r;
    std::copy_if(arr.begin(), arr.end(), std::back_inserter(r), [](int d) {
        return !(d % 2);
    });
    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << noOdds({1, 2, 3, 4, 5, 6, 7, 8}) << std::endl;
    std::cout << noOdds({43, 65, 23, 89, 53, 9, 6}) << std::endl;
    std::cout << noOdds({718, 991, 449, 644, 380, 440}) << std::endl;
}
