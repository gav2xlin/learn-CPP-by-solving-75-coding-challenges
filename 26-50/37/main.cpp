#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <cstdlib>

using namespace std;

int getAbsSum(std::vector<int> arr) {
    return std::accumulate(arr.begin(), arr.end(), 0, [](int r, int d) {
        return r + std::abs(d);
    });
}

int main() {
    std::cout << getAbsSum({2, -1, 4, 8, 10}) << std::endl;
    std::cout << getAbsSum({-3, -4, -10, -2, -3}) << std::endl;
    std::cout << getAbsSum({2, 4, 6, 8, 10}) << std::endl;
    std::cout << getAbsSum({-1}) << std::endl;
}
