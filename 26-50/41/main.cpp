#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<int> miniPeaks(vector<int> arr) {
    std::vector<int> r;
    for(int i = 1; i < arr.size() - 1; ++i) {
        if(arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) r.push_back(arr[i]);
    }
    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << miniPeaks({4, 5, 2, 1, 4, 9, 7, 2}) << std::endl;
    std::cout << miniPeaks({1, 2, 1, 1, 3, 2, 5, 4, 4}) << std::endl;
    std::cout << miniPeaks({1, 2, 3, 4, 5, 6}) << std::endl;
}
