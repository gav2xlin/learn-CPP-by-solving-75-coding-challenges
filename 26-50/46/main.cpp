#include <iostream>
#include <string>

using namespace std;

std::string longestZero(std::string str) {
    int l = 0;
    int c = 0;

    for (const auto& ch : str) {
        if (ch == '0') {
            ++c;
        } else {
            l = std::max(l, c);
            c = 0;
        }
    }

    return std::string(l, '0');
}

int main() {
    std::cout << longestZero("0100001011000") << std::endl;
    std::cout << longestZero("100100100") << std::endl;
    std::cout << longestZero("11111") << std::endl;
}
