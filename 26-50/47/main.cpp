#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool is_prime(int n) {
    if (n <= 3) {
        return n > 1;
    } else if (n % 2 == 0 || n % 3 == 0) {
        return false;
    } else {
        int i = 5;
        while (i * i <= n) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
            i += 6;
        }
        return true;
    }
}

std::vector<int> primesBelowNum(int n) {
    std::vector<int> r;

    for (int i = 2; i <= n; ++i) {
        if (is_prime(i)) r.push_back(i);
    }

    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << primesBelowNum(5) << std::endl;
    std::cout << primesBelowNum(10) << std::endl;
    std::cout << primesBelowNum(20) << std::endl;
}
