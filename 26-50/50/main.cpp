#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<std::vector<int>> squarePatch(int n) {
    return std::vector<std::vector<int>>(n, std::vector(n, n));
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << squarePatch(3) << std::endl;
    std::cout << squarePatch(5) << std::endl;
    std::cout << squarePatch(1) << std::endl;
    std::cout << squarePatch(0) << std::endl;
}
