#include <iostream>
#include <string>

using namespace std;

std::string ABA(char s){
  if (s == 'A') {
      return "A";
  }
  else {
      return ABA(s-1) + s + ABA(s-1);
  }
}

int main() {
  std::cout << ABA('A') << std::endl;
  std::cout << ABA('B') << std::endl;
  std::cout << ABA('E') << std::endl;
}
