#include <iostream>
#include <string>
#include <vector>

using namespace std;

int spinAround(std::vector<std::string> r) {
    int s = 0;
    for (const auto &d : r) {
        if (d == "right") s+= 90;
        else if (d == "left") s -= 90;
    }
    if (s < 0) s = -s;
    return s / 360;
}

int main() {
    std::cout << spinAround({"left", "right", "left", "right"}) << std::endl;
    std::cout << spinAround({"right", "right", "right", "right", "right", "right", "right", "right"}) << std::endl;
    std::cout << spinAround({"left", "left", "left", "left"}) << std::endl;
}
