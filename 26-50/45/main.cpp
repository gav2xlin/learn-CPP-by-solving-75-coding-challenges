#include <iostream>
#include <string>

using namespace std;

int days(int m, int y) {
    static int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (m != 2) {
        return days[m - 1];
    } else {
        return ((y % 4 == 0 && !(y % 100 == 0)) || y % 400 == 0) ? 29 : 28;
    }
}

int main() {
    std::cout << days(2, 2018) << std::endl;
    std::cout << days(4, 654) << std::endl;
    std::cout << days(2, 200) << std::endl;
    std::cout << days(2, 1000) << std::endl;
}
