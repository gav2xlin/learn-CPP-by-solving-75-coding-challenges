#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> amplify(int n) {
    std::vector<int> v;
    for (int i = 1; i <= n; ++i) {
        v.push_back((i % 4) ? i : 10*i);
    }
    return v;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << amplify(4) << std::endl;
    std::cout << amplify(3) << std::endl;
    std::cout << amplify(25) << std::endl;
}
