#include <iostream>
#include <string>

using namespace std;

int fibonacci(int num) {
    // if(num <= 1) return 1;
    // else return fibonacci(num-1)+fibonacci(num-2);
    if (num <= 2) return 1;

    int t0 = 0, t1 = 1, n;
    for (int i = 1; i <= num; ++i) {
        n = t0 + t1;
        t0 = t1;
        t1 = n;
    }
    return n;
}

int main() {
    std::cout << fibonacci(3) << std::endl;
    std::cout << fibonacci(7) << std::endl;
    std::cout << fibonacci(12) << std::endl;
}
