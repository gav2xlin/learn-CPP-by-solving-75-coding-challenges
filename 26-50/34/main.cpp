#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int countWords(std::string str) {
    return std::count(str.begin(), str.end(), ' ') + 1;
}

int main() {
   std::cout << countWords("Just an example here move along") << std::endl;
   std::cout << countWords("This is a test") << std::endl;
   std::cout << countWords("What an easy task, right") << std::endl;
}
