#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool changeEnough(std::vector<int> change, float amountDue) {
    float total = change.at(0) * 0.25 + change.at(1) * 0.1 + change.at(2) * 0.05 + change.at(3) * 0.01;
    return total >= amountDue;
}

int main() {
    std::cout << std::boolalpha << changeEnough({3, 100, 0, 0}, 14.11) << std::endl;
    std::cout << std::boolalpha << changeEnough({0, 0, 20, 5}, 0.75) << std::endl;
    std::cout << std::boolalpha << changeEnough({30, 40, 20, 5}, 12.55) << std::endl;
    std::cout << std::boolalpha << changeEnough({10, 0, 0, 50}, 3.85) << std::endl;
    std::cout << std::boolalpha << changeEnough({1, 0, 5, 219}, 19.99) << std::endl;
}
