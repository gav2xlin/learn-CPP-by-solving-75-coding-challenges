#include <iostream>
#include <string>
#include <vector>

using namespace std;

int countCharacters(std::vector<std::string> arr) {
  int c = 0;
  for (auto &i : arr) {
      for (const auto j : i) {
          if (j != ' ') {
              ++c;
          }
      }
  }
  return c;
}

int main() {
  std::cout << countCharacters({"###", "###", "###"}) << std::endl;
  std::cout << countCharacters({"22222222", "22222222"}) << std::endl;
  std::cout << countCharacters({"------------------"}) << std::endl;
  std::cout << countCharacters({}) << std::endl;
  std::cout << countCharacters({"", ""}) << std::endl;
}
