#include <iostream>
#include <string>

//using namespace std;

std::string move(std::string word) {
    for (auto &c : word) {
        c += 1;
    }
    return word;
}

int main() {
    std::cout << move("hello") << std::endl;
    std::cout << move("bye") << std::endl;
    std::cout << move("welcome") << std::endl;
}
