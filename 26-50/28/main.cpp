#include <iostream>
#include <string>
#include <vector>

//using namespace std;

bool testJackpot(std::vector<std::string> result) {
    for (int i = 0; i < result.size() - 1; ++i) {
        if (result[i] != result[i + 1]) return false;
    }
    return true;
}

int main() {
    std::cout << std::boolalpha << testJackpot({"@", "@", "@", "@"}) << std::endl;
    std::cout << testJackpot({"abc", "abc", "abc", "abc"}) << std::endl;
    std::cout << testJackpot({"SS", "SS", "SS", "SS"}) << std::endl;
    std::cout << testJackpot({"&&", "&", "&&&", "&&&&"}) << std::endl;
    std::cout << testJackpot({"SS", "SS", "SS", "Ss"}) << std::endl;
}
