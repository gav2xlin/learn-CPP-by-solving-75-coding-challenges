#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

std::vector<int> sortNumsAscending(std::vector<int> arr) {
    std::sort(arr.begin(), arr.end());
    return arr;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}


int main() {
    std::cout << sortNumsAscending({1, 2, 10, 50, 5}) << std::endl;
    std::cout << sortNumsAscending({80, 29, 4, -95, -24, 85}) << std::endl;
    std::cout << sortNumsAscending({}) << std::endl;
    }
