#include <iostream>
#include <string>

using namespace std;

std::string doubleChar(std::string str) {
    std::string s(str.length() * 2, ' ');
    for (int i = 0; i < str.length(); ++i) {
        s[i * 2] = str[i];
        s[i * 2 + 1] = str[i];
    }
    return s;
}

int main() {
    std::cout << doubleChar("String") << std::endl;
    std::cout << doubleChar("Hello World!") << std::endl;
    std::cout << doubleChar("1234!_ ") << std::endl;
}
