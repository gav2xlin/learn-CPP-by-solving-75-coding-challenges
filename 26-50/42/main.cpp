#include <iostream>
#include <string>

using namespace std;

std::string reverse(std::string str) {
    // if (str == "") return "";
    // return reverse(str.substr(1)) + str[0];
    std::string s;
    auto n = str.size();
    s.reserve(str.size());
    for (int i = 0; i < n; ++i) {
        s += str[n - i - 1];
    }
    return s;
}

int main() {
    std::cout << reverse("hello") << std::endl;
    std::cout << reverse("world") << std::endl;
    std::cout << reverse("a") << std::endl;
    std::cout << reverse("") << std::endl;
}
