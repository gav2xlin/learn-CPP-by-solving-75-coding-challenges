#include <iostream>
#include <string>

using namespace std;

int sumDigits(int a, int b) {
    int r = 0;
    while (a <= b) {
        int t = a;
        while (t > 0) {
            r += t % 10;
            t /= 10;
        }
        ++a;
    }
    return r;
}

int main() {
    std::cout << sumDigits(7, 8) << std::endl;
    std::cout << sumDigits(17, 20) << std::endl;
    std::cout << sumDigits(10, 12) << std::endl;
}
