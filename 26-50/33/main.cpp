#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int countVowels(std::string str) {
    return std::count_if(str.begin(), str.end(), [](char c) {
      switch(c) {
          case 'a':
          case 'e':
          case 'i':
          case 'o':
          case 'u':
            return true;
          default:
            return false;
      }
  });
}

int main() {
    std::cout << countVowels("Celebration") << std::endl;
    std::cout << countVowels("Palm") << std::endl;
    std::cout << countVowels("Prediction") << std::endl;
}
