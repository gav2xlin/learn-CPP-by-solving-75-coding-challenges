#include <iostream>
#include <string>
#include <vector>

using namespace std;

int count_ones(std::vector<std::vector<int>> matrix) {
  int r = 0;
  for (auto &v : matrix) {
      for (const auto d : v) {
          // #include <algorithm>
          // r += std::count(v.begin(), v.end(), 1);
          if (d == 1) ++r;
      }
  }
  return r;
}

int main() {
    std::cout << count_ones({{1, 0}, {0, 0}}) << std::endl; // 1
    std::cout << count_ones({{1, 1, 1}, {0, 0, 1}, {1, 1, 1}}) << std::endl; // 7
    std::cout << count_ones({{1, 2, 3}, {0, 2, 1}, {5, 7, 33}}) << std::endl; // 2
}
