#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <cctype>
#include <algorithm>

using namespace std;

std::string keywordCipher(std::string key, std::string message) {
    key += "abcdefghijklmnopqrstuvwxyz";
    string k = "";
    std::unordered_set<std::string> visited;
    for (int i = 0; i < key.length(); i++) {
        std::string c = std::string(1, key[i]);
        if (visited.find(c) == visited.end()) {
            k += key[i];
            visited.insert(c);
        }
    }

    std::transform(message.begin(), message.end(), message.begin(),
        [&k](char c)
        {
            if (std::isalpha(c))
                return k[c - 'a'];

            return c;
        });

    return message;
}

int main() {
    std::cout << keywordCipher("keyword", "abchij") << std::endl;
    std::cout << keywordCipher("purplepineapple", "abc") << std::endl;
    std::cout << keywordCipher("mubashir", "edabit") << std::endl;
}
