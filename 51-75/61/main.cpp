#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// https://en.wikipedia.org/wiki/Binomial_coefficient

int binomialCoefficient(int n, int k) {
    if (k < 0 || k > n) return 0;
    if (k > n - k) k = n - k;
    if (k == 0 || k == n) return 1;
    return binomialCoefficient(n - 1, k - 1) + binomialCoefficient(n - 1, k);
}

/*int binomialCoefficient(int n, int k) {
    if (k < 0 || k > n) return 0;
    if (k == 0 || k == n) return 1;
    k = std::min(k, n - k);
    int c = 1;
    for (int i = 0; i < k; ++i) {
        c = c * (n - i) / (i + 1);
    }
    return c;
}*/

/*int factorial(int n) {
    int f = 1;
    for(int i = 1; i <= n; ++i) {
       f *= i;
    }
    return f;
}

int binomialCoefficient(int n, int k) {
    return factorial(n) / (factorial(k) * factorial(n - k));
}*/

std::string formula(int n) {
    if(n == 0) return "1";

    std::string ans = "";
    bool negative = false;
    if(n < 0){
        ans += "1/(";
        negative = true;
        n = -n;
    }

    // c*a^kb^(n-k) + ...
    for(int k=n; k>=0; k--){
        int coeff = binomialCoefficient(n, k);
        if(coeff > 1) ans += std::to_string(coeff);
        if(k > 0) ans += "a";
        if(k > 1) ans += "^" + std::to_string(k);
        if(k < n) ans += "b";
        if(k < n - 1) ans += "^" + std::to_string(n - k);
        if(k > 0) ans += "+";
    }

    if(negative) ans += ")";

    return ans;
}

int main() {
    std::cout << formula(0) << std::endl; // 1
    std::cout << formula(1) << std::endl; // a+b
    std::cout << formula(2) << std::endl; // a^2+2ab+b^2
    std::cout << formula(-2) << std::endl; // 1/(a^2+2ab+b^2)
    std::cout << formula(3) << std::endl; // a^3+3a^2b+3ab^2+b^3
    std::cout << formula(5) << std::endl; // a^5+5a^4b+10a^3b^2+10a^2b^3+5ab^4+b^5
}
