#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::string coconutTranslator(std::string const& str) {
    auto coconut = [](char n) {
        std::string s("coconuts");
        int i = 7;
        while (n > 0) {
            if (n & 1) {
                s[i] = std::toupper(s[i]);
            }
            n >>= 1;
            --i;
        }

        return s;
    };

    std::string encoded;
    for (auto c : str) {
        encoded += coconut(c) + ' ';
    }

    if (!encoded.empty()) {
        encoded.pop_back();
    }

    return encoded;
}

int main() {
    std::cout << coconutTranslator("Hi") << std::endl;
    std::cout << coconutTranslator("edabit") << std::endl;
    std::cout << coconutTranslator("123") << std::endl;
}
