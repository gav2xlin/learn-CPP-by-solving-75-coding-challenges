#include <iostream>
#include <string>

using namespace std;

std::string reverse(std::string str) {
    std::string r(str.size(), ' ');
    int n = str.size(), j = n - 1;
    for (int i = 0; i < n; ++i) {
        if ('0' <= str[i] && str[i] <= '9') {
            r[i] = str[i];
        }
    }
    for (int i = 0; i < n; ++i) {
        if ('0' > str[i] || str[i] > '9') {
            while (j >= 0&& r[j] != ' ') {
                --j;
            }
            if (j >= 0) {
              r[j] = str[i];
            }
        }
    }
    return r;
}

int main() {
    std::cout << reverse("ab98c") << std::endl;
    std::cout << reverse("jkl5mn923o") << std::endl;
    std::cout << reverse("123a45") << std::endl;
}
