#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <cctype>

using namespace std;

string condiCipher(string msg, string key, int shift) {
    key += "abcdefghijklmnopqrstuvwxyz";
    string k = "";
    std::unordered_set<std::string> visited;
    for (int i = 0; i < key.length(); i++) {
        std::string c = std::string(1, key[i]);
        if (visited.find(c) == visited.end()) {
            k += key[i];
            visited.insert(c);
        }
    }

    string encoded = "";
    for (int i = 0; i < msg.length(); ++i) {
        if (std::isalpha(msg[i])) {
            size_t idx = k.find(msg.substr(i, 1));
            encoded += k[(idx + shift) % 26];
            shift = idx+1;
        } else {
            encoded += msg[i];
        }
    }

    return encoded;
}

int main() {
    std::cout << condiCipher("on.,", "cryptogram", 10) << std::endl;
    std::cout << condiCipher("mubashir", "airforce", 6) << std::endl;
}
