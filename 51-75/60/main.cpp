#include <iostream>
#include <string>
//#include <regex>
#include <cctype>

using namespace std;

bool isValidHexCode(std::string str) {
    /*static std::regex e("#[A-Fa-f0-9]{6}");
    std::smatch m;

    std::regex_match(str, m, e);

    return !m.empty();*/

    if (str.empty() || str[0] != '#' || str.size() != 7) return false;

    for (int i = 1; i < str.size(); ++i) {
        if (!std::isxdigit(str[i])) return false;
    }

    return true;
}

int main() {
    std::cout << std::boolalpha << isValidHexCode("#CD5C5C") << std::endl;
    std::cout << isValidHexCode("#EAECEE") << std::endl;
    std::cout << isValidHexCode("#eaecee") << std::endl;
    std::cout << isValidHexCode("CD5C58C") << std::endl;
    std::cout << isValidHexCode("#CD5CSZ") << std::endl;
    std::cout << isValidHexCode("CD5C&C") << std::endl;
    std::cout << isValidHexCode("CD5C5C") << std::endl;
}
