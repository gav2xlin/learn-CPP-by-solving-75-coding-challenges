#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

std::string majorityVote(std::vector<std::string> arr) {
    std::sort(arr.begin(), arr.end());

    std::string max_vote = "", cur_vote = "";
    int max_count = 0, cur_count = 0;
    for (int i = 0; i < arr.size(); ++i) {
        if (arr[i] != cur_vote) {
            if (cur_count > max_count) {
               max_count = cur_count;
               max_vote = cur_vote;
            }
            cur_count = 0;
        }

        ++cur_count;
        cur_vote = arr[i];
    }

    return max_count > arr.size() / 2 ? max_vote : "None";
}


int main() {
    std::cout << majorityVote({"A", "A", "B"}) << std::endl;
    std::cout << majorityVote({"A", "A", "A", "B", "C", "A"}) << std::endl;
    std::cout << majorityVote({"A", "B", "B", "A", "C", "C"}) << std::endl;
}
