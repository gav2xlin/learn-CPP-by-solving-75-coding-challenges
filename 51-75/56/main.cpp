#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cmath>

using namespace std;

bool checkExp(std::string exp) {
    int i = exp.find('=');
    int c = std::stoi(exp.substr(i + 1));
    std::string e = exp.substr(0, i);

    char o;
    bool f{};
    for (char s : {'-', '+'}) {
        i = e.find(s);
        if (i >= 0) {
            o = s;
            f = true;
            break;
        }
    }

    if (!f) {
        throw std::runtime_error("unknown operation: " + std::string(1, c));
    }

    int a = std::stoi(e.substr(i - 1));
    int b = std::stoi(e.substr(i + 1));

    int v{};

    switch (o) {
    case '-': v = a - b; break;
    case '+': v = a + b; break;
    }

    return v == c;
}

std::string markMaths(std::vector<std::string> arr) {
    int r = 0;

    for (const auto& e : arr) {
        if (checkExp(e)) ++r;
    }

    std::ostringstream so;

    so << static_cast<int>(std::round(static_cast<double>(r) / arr.size() * 100)) << '%';

    return so.str();
}

int main() {
    std::cout << markMaths({"2+2=4", "3+2=5", "10-3=3", "5+5=10"}) << std::endl; // 75%
    std::cout << markMaths({"1-2=2"}) << std::endl; // 0%
    std::cout << markMaths({"2+3=5", "4+4=9", "3-1=2"}) << std::endl; // 67%
}
