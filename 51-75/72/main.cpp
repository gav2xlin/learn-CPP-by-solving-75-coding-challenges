#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

string longestCommonEnding(string s1, string s2) {
    int sz = std::min(s1.length(), s2.length()), n = sz;
    auto p1 = s1.end(), p2 = s2.end();
    for (int i = 0; i < sz; ++i, --n) {
        if (*(--p1) != *(--p2)) {
            break;
        }
    }
    return {p1 + (n > 0 ? 1 : 0), s1.end()};
}

int main() {
    std::cout << longestCommonEnding("multiplication", "ration") << std::endl; // ation
    std::cout << longestCommonEnding("potent", "tent") << std::endl; // tent
    std::cout << longestCommonEnding("tent", "potent") << std::endl; // tent
    std::cout << longestCommonEnding("skyscraper", "carnivore") << std::endl; //
    std::cout << longestCommonEnding("vote", "asymptote") << std::endl; // ote
}
