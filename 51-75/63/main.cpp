#include <iostream>
#include <string>
#include <vector>
#include <functional>

using namespace std;

std::vector<std::string> numbersToRanges(std::vector<int> arr) {
    std::vector<std::string> ans;

    std::function<std::string(int, int)> interval = [](int l, int r) {
        return std::to_string(l) + (l != r ? "-" + std::to_string(r) : "");
    };

    int l = arr[0], r = l;

    for (int i = 1; i < arr.size(); ++i) {
        if (arr[i] != arr[i - 1] + 1) {
            ans.push_back(interval(l, r));

            l = arr[i];
        }
        r = arr[i];
    }

    ans.push_back(interval(l, r));

    return ans;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << numbersToRanges({1, 2, 3, 4, 5}) << std::endl; // 1-5
    std::cout << numbersToRanges({3, 4, 5, 10, 11, 12}) << std::endl; // 3-5 10-12
    std::cout << numbersToRanges({1, 2, 3, 4, 99, 100}) << std::endl; // 1-4 99-100
    std::cout << numbersToRanges({1, 3, 4, 5, 6, 7, 8}) << std::endl; // 1 3-8
}
