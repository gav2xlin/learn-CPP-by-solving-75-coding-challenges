#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

std::vector<int> largerThanRight(std::vector<int> arr) {
    std::vector<int> r;
    r.reserve(arr.size());

    int m = -1;

    for (auto it = arr.rbegin(); it != arr.rend(); ++it) {
        if (*it > m) {
            r.push_back(*it);
            m = *it;
        }
    }

    std::reverse(r.begin(), r.end());

    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << largerThanRight({3, 13, 11, 2, 1, 9, 5}) << std::endl;
    std::cout << largerThanRight({5, 5, 5, 5, 5, 5}) << std::endl;
    std::cout << largerThanRight({5, 9, 8, 7}) << std::endl;
}
