#include <iostream>
#include <string>
#include <vector>

using namespace std;

int longestRun(std::vector<int> arr) {
    int r = 1, c = 0;
    bool f{true};

    for(int i = 1; i < arr.size(); ++i) {
        if (arr[i] != arr[i - 1] + 1 && arr[i] != arr[i - 1] - 1) {
            if (c > r) {
                r = c + 1;
            }
            c = 0;
            f = false;
        }
        ++c;
    }

    if (c > r) {
        r = f ? c + 1 : c;
    }

    return r;
}

int main() {
    std::cout << longestRun({1, 2, 3, 5, 6, 7, 8, 9}) << std::endl; // 5
    std::cout << longestRun({1, 2, 3, 10, 11, 15}) << std::endl; // 3
    std::cout << longestRun({5, 4, 2, 1}) << std::endl; // 2
    std::cout << longestRun({3, 5, 7, 10, 15}) << std::endl; // 1
}
