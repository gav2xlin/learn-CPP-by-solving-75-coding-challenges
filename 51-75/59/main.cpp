#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<std::string> leftRotations(std::string str) {
    std::vector<std::string> r;

    for (int i = 0; i < str.size(); ++i) {
        // #include <algorithm>
        // std::string s{str};
        // std::rotate(s.begin(), s.begin() + i, s.end());
        r.push_back(str.substr(i) + str.substr(0, i));
    }

    return r;
}

std::vector<std::string> rightRotations(std::string str) {
    std::vector<std::string> r;

    size_t n = str.size();
    for (int i = 0; i < n; ++i) {
        // #include <algorithm>
        // std::string s{str};
        // std::rotate(s.rbegin(), s.rbegin() + i, s.rend());
        auto j = n - i;
        r.push_back(str.substr(j) + str.substr(0, j));
    }

    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << leftRotations("abc") << std::endl;
    std::cout << rightRotations("abc") << std::endl;
    std::cout << leftRotations("abcdef") << std::endl;
    std::cout << rightRotations("abcdef") << std::endl;
}
