#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <cctype>

using namespace std;

std::string ragbabyCipher(std::string message, std::string key) {
    key += "abcdefghijklmnopqrstuvwxyz";
    string k = "";
    std::unordered_set<std::string> visited;
    for (int i = 0; i < key.length(); i++) {
        std::string c = std::string(1, key[i]);
        if (visited.find(c) == visited.end()) {
            k += key[i];
            visited.insert(c);
        }
    }

    string encoded = "";
    int i = 0;
    while (i < message.length()) {
        if (std::isalpha(message[i])) {
            std::string s = "";
            while (i < message.length() && std::isalpha(message[i])) {
                s += message[i];
                ++i;
            }

            for (int j = 0; j < s.length(); ++j) {
                size_t c = k.find(std::tolower(s[j]));
                char ch = k[(c + j + 1) % 26];
                if (std::isupper(s[j])) {
                    ch = std::toupper(ch);
                }
                encoded += ch;
             }
        } else {
            encoded += message[i];
            ++i;
        }
    }
    return encoded;
}

int main() {
    std::cout << ragbabyCipher("This is an example.", "cipher") << std::endl;
    std::cout << ragbabyCipher("m.u^b#a@s!h4ir", "helen") << std::endl;
}
