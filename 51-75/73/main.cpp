#include <iostream>
#include <string>
#include <vector>

using namespace std;

int rySeq(int n, std::string s) {
    // 1 - r1 + y0 = a1
    // 2 - r3 + y2 = a5
    // 3 - r5 + y8 = a13
    // 4 - r7 + y18 = a25

    // 1 -                      1(1)
    // 2 -               1(1) + 3(2) + 1
    // 3 -        1(1) + 3(1) + 5(3) + 3 + 1
    // 4 - 1(1) + 3(1) + 5(1) + 7(4) + 5 + 3 + 1
    if (!n) return 0;

    int r = n * 2 - 1;
    if(s == "red") return r;

    int a = 1;
    int b = 4;

    for(int i = 1; i < n ; ++i) {
        a += b;
        b += 4;
    }

    if (s == "all") return a;
    if (s == "yellow") return a - r;

    return - 1;
}

int main() {
    std::cout << rySeq(2, "all") << std::endl; // 5
    std::cout << rySeq(3, "yellow") << std::endl; // 8
    std::cout << rySeq(28, "red") << std::endl; // 55
    std::cout << rySeq(1, "blue") << std::endl; // -1
}
