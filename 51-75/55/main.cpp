#include <iostream>
#include <string>
#include <unordered_set>
#include <sstream>

using namespace std;

std::string happyAlgorithm(int num) {
    std::unordered_set<int> visited;

    int step = 0;
    bool happy = true;

    while (true) {
        int t = num, s = 0;
        while (t > 0) {
           int r = t % 10;
           s += r * r;
           t /= 10;
        }
        num = s;

        if (visited.find(num) != visited.end()) {
            happy = false;
            break;
        }

        ++step;

        if (num == 1) break;

        visited.insert(num);
    }

    std::stringstream ss;
    ss << (happy ? "HAPPY " : "SAD ") << step;

    return ss.str();
}

int main() {
    std::cout << happyAlgorithm(139) << std::endl;
    std::cout << happyAlgorithm(67) << std::endl;
    std::cout << happyAlgorithm(1) << std::endl;
    std::cout << happyAlgorithm(89) << std::endl;
}
