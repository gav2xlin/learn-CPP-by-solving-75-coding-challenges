#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<std::string> splitGroups(std::string s) {
    std::vector<std::string> r;

    int j = 1;
    for (int i = 1; i < s.length(); ++i) {
        if (s[i] != s[i - 1]) {
            r.emplace_back(j, s[i - 1]);
            j = 0;
        }
        ++j;
    }

    if (!s.empty()) {
        r.emplace_back(j, s[s.length() - 1]);
    }

    return r;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << splitGroups("5556667788") << std::endl;
    std::cout << splitGroups("aaabbbaabbab") << std::endl;
    std::cout << splitGroups("abbbcc88999&&!!!_") << std::endl;
    std::cout << splitGroups("a") << std::endl;
}
