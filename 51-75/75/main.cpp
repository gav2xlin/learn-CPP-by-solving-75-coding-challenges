#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int battleOutcome(int num) {
    int n = 0;
    while (num > 0) {
        n = n * 10 + num % 10;
        num /= 10;
    }

    int r = 0;
    while (n > 0) {
        int p = n % 100;
        int d1 = p / 10;
        int d2 = p % 10;
        if (d1 != d2) {
            r = r * 10 + std::max(d1, d2);
        }
        n /= 100;
    }
    return r;
}

int main() {
    std::cout << battleOutcome(123) << std::endl; // 23
    std::cout << battleOutcome(578921445) << std::endl; // 7925
    std::cout << battleOutcome(32531) << std::endl; // 351
    std::cout << battleOutcome(111) << std::endl; // 1
    std::cout << battleOutcome(78925) << std::endl; // 895
}
