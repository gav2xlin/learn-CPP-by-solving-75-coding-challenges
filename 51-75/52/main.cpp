#include <iostream>
#include <string>
#include <vector>

using namespace std;

char cupSwapping(std::vector<std::string> swaps) {
    char answer = 'B';
    for (int i = 0; i < swaps.size(); ++i) {
        if (swaps[i][0] == answer) {
            answer = swaps[i][1];
        } else if (swaps[i][1] == answer) {
            answer = swaps[i][0];
        }
    }
    return answer;
}

int main() {
    std::cout << cupSwapping({"AB", "CA", "AB"}) << std::endl;
    std::cout << cupSwapping({"AB", "CA"}) << std::endl;
    std::cout << cupSwapping({"AC", "CA", "CA", "AC"}) << std::endl;
    std::cout << cupSwapping({"BA", "AC", "CA", "BC"}) << std::endl;
}
