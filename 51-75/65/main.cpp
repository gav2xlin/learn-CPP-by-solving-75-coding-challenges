#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

int connellSequence(int start, int end, int n) {
    // Connell sequence: num * (num - 2) + 2 * i (0 .. num - 1)
    // 1) 1 (0) 0 * 0 = 0 [0]
    // 2) 2 4 (1 2) 1 * 1 = 1 [1]
    // 3) 5 7 9 (3 4 5) 3 * 1 = 3 [1]
    // 4) 10 12 14 16 (6 7 8 9) 3 * 2 = 6 [2]
    // 5) 17 19 21 23 25 (10 11 12 13 14) 5 * 2 = 10 [2]
    // 6) 26 28 30 32 34 36 (?) (15 16 17 18 19 20) 5 * 3 = 15 [3]
    // 7) 37 39 41 43 45 47 49 (?) (21 22 23 24 25 26 27) 7 * 3 = 21 [3]
    // 8) 50 52 54 56 58 60 62 64 (?) (28 29 30 31 32 33 34 35) 7 * 4 = 28 [4]
    // 9) 65 67 69 71 73 75 77 79 81 (?) (36 37 38 39 40 41 42 43 44) 9 * 4 = 36 [4]
    // 10) 82 84 86 88 90 92 94 96 98 100 (?) (45 46 47 48 49 50 51 52 53 54) 9 * 5 = 45 [5]

    int row{static_cast<int>(std::ceil(std::sqrt(n)))};

    if (start > row || row > end || row % 2 != n % 2) return -1; // a row and n are both odd or even

    int idx = 0;
    for (int i = start; i < end; ++i) {
        int d = i * (i - 2) + 2;
        for (int j = 0; j < i; ++j) {
            if (d == n) return idx;
            d += 2;
            ++idx;
        }
    }

    return idx;
    // return ( row + start - start * start + n - 2 ) / 2;
}

int main() {
    std::cout << connellSequence(1, 3, 4) << std::endl; // 2
    std::cout << connellSequence(2, 3, 4) << std::endl; // 1
    std::cout << connellSequence(4, 5, 22) << std::endl; // -1
}
