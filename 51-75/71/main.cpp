#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int sumMissingNumbers(std::vector<int> arr) {
    std::sort(arr.begin(), arr.end());

    int sum = 0;
    for (int i = 1; i < arr.size(); ++i) {
        for (int j = arr[i - 1] + 1; j < arr[i]; ++j) {
            sum += j;
        }
    }
    return sum;
}

int main() {
    std::cout << sumMissingNumbers({1, 3, 5, 7, 10}) << std::endl; // 29
    std::cout << sumMissingNumbers({10, 7, 5, 3, 1}) << std::endl; // 29
    std::cout << sumMissingNumbers({10, 20, 30, 40, 50, 60}) << std::endl; // 1575
}
