#include <iostream>
#include <string>
#include <vector>

using namespace std;

int countLayers(vector<string> rug) {
    string strip = rug[rug.size() / 2];

    char c = strip[0];
    int l = 0, n = strip.size() / 2 + 1;

    for (int i = 0; i < n; ++i) {
        if (strip[i] != c) {
            c = strip[i];
            ++l;
        }
    }

    return l + 1;
}

int main() {
    std::cout << countLayers({"AAAA",
                              "ABBA",
                              "AAAA"}) << std::endl; // 2
    std::cout << countLayers({"AAAAAAAA",
                              "ABBBBBBA",
                              "ABBAABBA",
                              "ABBBBBBA",
                              "AAAAAAAA"}) << std::endl; // 3
    std::cout << countLayers({"AAAAAAAAAAA",
                              "AABBBBBBBAA",
                              "AABCCCCCBAA",
                              "AABCAAACBAA",
                              "AABCADACBAA",
                              "AABCAAACBAA",
                              "AABCCCCCBAA",
                              "AABBBBBBBAA",
                              "AAAAAAAAAAA"}) << std::endl; // 5

}
