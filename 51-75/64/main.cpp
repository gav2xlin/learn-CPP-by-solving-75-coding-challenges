#include <iostream>
#include <string>
#include <vector>

using namespace std;

void seedGrass(vector<string>& g, int x, int y) {
    if (y < 0 || y >= g.size()) return;
    if (x < 0 || x >= g[y].size()) return;

    if (g[y][x] == '.') {
        g[y][x] = '+';

        seedGrass(g, x - 1, y);
        seedGrass(g, x + 1, y);
        seedGrass(g, x, y - 1);
        seedGrass(g, x, y + 1);
    }
}

vector<string> simulateGrass(vector<string> g, int x, int y) {
    seedGrass(g, x, y);
    return g;
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

template <>
std::ostream& operator<< (std::ostream& os, const std::vector<std::string>& v) {
    for (const auto& d : v) {
        os << d << '\n';
    }
    return os;
}

int main() {
    // xxxxxxx
    // x+++++x
    // xxxx++x
    // x...xxx
    // xxxxxxx
    std::cout << simulateGrass({"xxxxxxx",
                                "x.....x",
                                "xxxx..x",
                                "x...xxx",
                                "xxxxxxx"}, 1, 1) << std::endl;
}
