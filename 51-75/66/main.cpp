#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
#include <stdexcept>

using namespace std;

std::string hillCipher(std::string message, std::string key) {
    if (key.length() != 4) throw std::runtime_error("the key length is always 4");

    std::transform(message.begin(), message.end(), message.begin(), ::toupper);
    std::transform(key.begin(), key.end(), key.begin(), ::toupper);

    int km[][2] = {{key[0] - 'A', key[1] - 'A'},
                   {key[2] - 'A', key[3] - 'A'}};

    std::string::iterator it = std::remove_if(message.begin(), message.end(), [](char c) {
        return 'A' > c || c > 'Z';
    });
    message.erase(it, message.end());

    if (message.length() % 2 != 0) message += 'Z';

    std::string encoded(message.length(), ' ');
    for (int i = 0,j = 0; i < message.length() / 2; ++i) {
        int a = message[i * 2] - 'A', b = message[i * 2 + 1] - 'A';
        int x = (km[0][0] * a + km[0][1] * b) % 26;
        int y = (km[1][0] * a + km[1][1] * b) % 26;
        encoded[j++] = char(x + 'A');
        encoded[j++] = char(y + 'A');
    }

    return encoded;
}

int main() {
    std::cout << hillCipher("H& *i", "cats") << std::endl; // OR
    std::cout << hillCipher("mubashir", "matt") << std::endl; // OKMTIHSH
    std::cout << hillCipher("Five + Seven = Twelve", "math") << std::endl; // OKMTIHSH
}
