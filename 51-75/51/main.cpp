#include <iostream>
#include <string>
#include <vector>

using namespace std;

std::vector<int> calculateYears(int humanYears) {
    int cat=0, dog=0;
    for(int i=0; i < humanYears; ++i) {
        if(i == 0) {
            cat += 15;
            dog += 15;
        } else if(i == 1) {
            cat += 9;
            dog += 9;
        } else {
            cat += 4;
            dog += 5;
        }
    }
    return {humanYears, cat, dog};
}

template <typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v) {
    for (const auto& d : v) {
        os << d << ' ';
    }
    return os;
}

int main() {
    std::cout << calculateYears(1) << std::endl;
    std::cout << calculateYears(2) << std::endl;
    std::cout << calculateYears(10) << std::endl;
}
